package com.freetty.config;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.context.web.SpringBootServletInitializer;
import org.springframework.boot.orm.jpa.EntityScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;


@SpringBootApplication(scanBasePackages="com.freetty")
@EntityScan(basePackages={"com.freetty.entity"})
@EnableJpaRepositories(basePackages={"com.freetty.repository"})
public class ApplicationConfig extends SpringBootServletInitializer {

	public static void main(String[] args) throws Exception {
		SpringApplication.run(ApplicationConfig.class, args);
	}
	
	
	/*외부 톰캣컨테이너에서 스프링 부트를 사용하기 위한 스프링 어플리케이션 빌더 설정*/
	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder builder) {
		return builder.sources(ApplicationConfig.class);
	}
	

}