package com.freetty.entity;

import java.io.Serializable;
import javax.persistence.*;

/**
 * The primary key class for the artist_license database table.
 * 
 */
@Embeddable
public class ArtistLicensePK implements Serializable {
	//default serial version id, required for serializable classes.
	private static final long serialVersionUID = 1L;

	@Column(name="idf_artist", insertable=false, updatable=false, unique=true, nullable=false)
	private int idfArtist;

	@Column(name="idf_artist_license", unique=true, nullable=false)
	private int idfArtistLicense;

	public ArtistLicensePK() {
	}
	public int getIdfArtist() {
		return this.idfArtist;
	}
	public void setIdfArtist(int idfArtist) {
		this.idfArtist = idfArtist;
	}
	public int getIdfArtistLicense() {
		return this.idfArtistLicense;
	}
	public void setIdfArtistLicense(int idfArtistLicense) {
		this.idfArtistLicense = idfArtistLicense;
	}

	public boolean equals(Object other) {
		if (this == other) {
			return true;
		}
		if (!(other instanceof ArtistLicensePK)) {
			return false;
		}
		ArtistLicensePK castOther = (ArtistLicensePK)other;
		return 
			(this.idfArtist == castOther.idfArtist)
			&& (this.idfArtistLicense == castOther.idfArtistLicense);
	}

	public int hashCode() {
		final int prime = 31;
		int hash = 17;
		hash = hash * prime + this.idfArtist;
		hash = hash * prime + this.idfArtistLicense;
		
		return hash;
	}
}