package com.freetty.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Timestamp;


/**
 * The persistent class for the artist_license database table.
 * 
 */
@Entity
@Table(name="artist_license")
@NamedQuery(name="ArtistLicense.findAll", query="SELECT a FROM ArtistLicense a")
public class ArtistLicense  {
	private static final long serialVersionUID = 1L;

	@EmbeddedId
	private ArtistLicensePK id;

	private byte confirm;

	@Column(name="image_license", length=100)
	private String imageLicense;

	@Column(name="reg_date", insertable=false, updatable=false, nullable=false)
	private Timestamp regDate;

	//bi-directional many-to-one association to Artist
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="idf_artist", nullable=false, insertable=false, updatable=false)
	private Artist artist;

	public ArtistLicense() {
	}

	public ArtistLicensePK getId() {
		return this.id;
	}

	public void setId(ArtistLicensePK id) {
		this.id = id;
	}

	public byte getConfirm() {
		return this.confirm;
	}

	public void setConfirm(byte confirm) {
		this.confirm = confirm;
	}

	public String getImageLicense() {
		return this.imageLicense;
	}

	public void setImageLicense(String imageLicense) {
		this.imageLicense = imageLicense;
	}

	public Timestamp getRegDate() {
		return this.regDate;
	}

	public void setRegDate(Timestamp regDate) {
		this.regDate = regDate;
	}

	public Artist getArtist() {
		return this.artist;
	}

	public void setArtist(Artist artist) {
		this.artist = artist;
	}

}