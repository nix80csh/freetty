package com.freetty.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Timestamp;


/**
 * The persistent class for the artist_major database table.
 * 
 */
@Entity
@Table(name="artist_major")
@NamedQuery(name="ArtistMajor.findAll", query="SELECT a FROM ArtistMajor a")
public class ArtistMajor  {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="idf_artist", unique=true, nullable=false)
	private int idfArtist;

	private byte baber;

	private byte esthetic;

	private byte hair;

	private byte lash;

	private byte makeup;

	private byte nail;

	@Column(name="reg_date", insertable=false, updatable=false, nullable=false)
	private Timestamp regDate;

	@Column(name="service_description", length=100)
	private String serviceDescription;

	private byte tattoo;

	private byte waxing;

	//bi-directional one-to-one association to Artist
	@OneToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="idf_artist", nullable=false, insertable=false, updatable=false)
	private Artist artist;

	public ArtistMajor() {
	}

	public int getIdfArtist() {
		return this.idfArtist;
	}

	public void setIdfArtist(int idfArtist) {
		this.idfArtist = idfArtist;
	}

	public byte getBaber() {
		return this.baber;
	}

	public void setBaber(byte baber) {
		this.baber = baber;
	}

	public byte getEsthetic() {
		return this.esthetic;
	}

	public void setEsthetic(byte esthetic) {
		this.esthetic = esthetic;
	}

	public byte getHair() {
		return this.hair;
	}

	public void setHair(byte hair) {
		this.hair = hair;
	}

	public byte getLash() {
		return this.lash;
	}

	public void setLash(byte lash) {
		this.lash = lash;
	}

	public byte getMakeup() {
		return this.makeup;
	}

	public void setMakeup(byte makeup) {
		this.makeup = makeup;
	}

	public byte getNail() {
		return this.nail;
	}

	public void setNail(byte nail) {
		this.nail = nail;
	}

	public Timestamp getRegDate() {
		return this.regDate;
	}

	public void setRegDate(Timestamp regDate) {
		this.regDate = regDate;
	}

	public String getServiceDescription() {
		return this.serviceDescription;
	}

	public void setServiceDescription(String serviceDescription) {
		this.serviceDescription = serviceDescription;
	}

	public byte getTattoo() {
		return this.tattoo;
	}

	public void setTattoo(byte tattoo) {
		this.tattoo = tattoo;
	}

	public byte getWaxing() {
		return this.waxing;
	}

	public void setWaxing(byte waxing) {
		this.waxing = waxing;
	}

	public Artist getArtist() {
		return this.artist;
	}

	public void setArtist(Artist artist) {
		this.artist = artist;
	}

}