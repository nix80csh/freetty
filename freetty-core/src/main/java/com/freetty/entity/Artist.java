package com.freetty.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Timestamp;
import java.util.List;


/**
 * The persistent class for the artist database table.
 * 
 */
@Entity
@Table(name="artist")
@NamedQuery(name="Artist.findAll", query="SELECT a FROM Artist a")
public class Artist  {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="idf_artist", unique=true, nullable=false)
	private int idfArtist;

	@Column(length=8)
	private String birthday;

	@Column(length=45)
	private String email;

	@Column(length=1)
	private String gender;

	@Column(length=500)
	private String introduce;

	@Column(name="language_chi")
	private byte languageChi;

	@Column(name="language_eng")
	private byte languageEng;

	@Column(name="language_jap")
	private byte languageJap;

	@Column(name="language_sign")
	private byte languageSign;

	@Column(length=11)
	private String mobile;

	@Column(length=45)
	private String name;

	@Column(length=45)
	private String nickname;

	@Column(length=64)
	private String password;

	@Column(name="reg_date", insertable=false, updatable=false, nullable=false)
	private Timestamp regDate;

	@Column(name="service_distance")
	private byte serviceDistance;

	//bi-directional many-to-one association to ArtistLicense
	@OneToMany(mappedBy="artist")
	private List<ArtistLicense> artistLicenses;

	//bi-directional one-to-one association to ArtistMajor
	@OneToOne(mappedBy="artist", fetch=FetchType.LAZY)
	private ArtistMajor artistMajor;

	//bi-directional one-to-one association to ArtistSchedule
	@OneToOne(mappedBy="artist", fetch=FetchType.LAZY)
	private ArtistSchedule artistSchedule;

	//bi-directional one-to-one association to ArtistShop
	@OneToOne(mappedBy="artist", fetch=FetchType.LAZY)
	private ArtistShop artistShop;

	public Artist() {
	}

	public int getIdfArtist() {
		return this.idfArtist;
	}

	public void setIdfArtist(int idfArtist) {
		this.idfArtist = idfArtist;
	}

	public String getBirthday() {
		return this.birthday;
	}

	public void setBirthday(String birthday) {
		this.birthday = birthday;
	}

	public String getEmail() {
		return this.email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getGender() {
		return this.gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getIntroduce() {
		return this.introduce;
	}

	public void setIntroduce(String introduce) {
		this.introduce = introduce;
	}

	public byte getLanguageChi() {
		return this.languageChi;
	}

	public void setLanguageChi(byte languageChi) {
		this.languageChi = languageChi;
	}

	public byte getLanguageEng() {
		return this.languageEng;
	}

	public void setLanguageEng(byte languageEng) {
		this.languageEng = languageEng;
	}

	public byte getLanguageJap() {
		return this.languageJap;
	}

	public void setLanguageJap(byte languageJap) {
		this.languageJap = languageJap;
	}

	public byte getLanguageSign() {
		return this.languageSign;
	}

	public void setLanguageSign(byte languageSign) {
		this.languageSign = languageSign;
	}

	public String getMobile() {
		return this.mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getNickname() {
		return this.nickname;
	}

	public void setNickname(String nickname) {
		this.nickname = nickname;
	}

	public String getPassword() {
		return this.password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Timestamp getRegDate() {
		return this.regDate;
	}

	public void setRegDate(Timestamp regDate) {
		this.regDate = regDate;
	}

	public byte getServiceDistance() {
		return this.serviceDistance;
	}

	public void setServiceDistance(byte serviceDistance) {
		this.serviceDistance = serviceDistance;
	}

	public List<ArtistLicense> getArtistLicenses() {
		return this.artistLicenses;
	}

	public void setArtistLicenses(List<ArtistLicense> artistLicenses) {
		this.artistLicenses = artistLicenses;
	}

	public ArtistLicense addArtistLicens(ArtistLicense artistLicens) {
		getArtistLicenses().add(artistLicens);
		artistLicens.setArtist(this);

		return artistLicens;
	}

	public ArtistLicense removeArtistLicens(ArtistLicense artistLicens) {
		getArtistLicenses().remove(artistLicens);
		artistLicens.setArtist(null);

		return artistLicens;
	}

	public ArtistMajor getArtistMajor() {
		return this.artistMajor;
	}

	public void setArtistMajor(ArtistMajor artistMajor) {
		this.artistMajor = artistMajor;
	}

	public ArtistSchedule getArtistSchedule() {
		return this.artistSchedule;
	}

	public void setArtistSchedule(ArtistSchedule artistSchedule) {
		this.artistSchedule = artistSchedule;
	}

	public ArtistShop getArtistShop() {
		return this.artistShop;
	}

	public void setArtistShop(ArtistShop artistShop) {
		this.artistShop = artistShop;
	}

}