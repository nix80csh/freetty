package com.freetty.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.freetty.entity.Artist;

public interface ArtistRepo extends JpaRepository<Artist, Integer> {
	public Artist findByEmail(String email);
}
