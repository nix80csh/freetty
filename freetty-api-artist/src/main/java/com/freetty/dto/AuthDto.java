package com.freetty.dto;

import org.hibernate.validator.constraints.NotBlank;

import lombok.Data;


@Data
public class AuthDto {
	private Integer idfArtist;
	
	@NotBlank
	private String email;
	
	@NotBlank		
	private String password;
	
	@Data
	public static class ValidateEmail{
		private String email;
	}
}
