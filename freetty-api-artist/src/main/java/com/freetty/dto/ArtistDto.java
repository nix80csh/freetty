package com.freetty.dto;



import javax.validation.constraints.NotNull;

import org.springframework.web.multipart.MultipartFile;

import lombok.Data;

@Data
public class ArtistDto {
	
	private Integer idfArtist;	
	private String email;
	private String name;
	private String nickname;
	private String birthday;
	private String gender;
	private String mobile;
	private String introduce;
	private byte languageChi;
	private byte languageEng;
	private byte languageJap;
	private byte languageSign;
	
	@Data
	public static class saveProfileImage{
		
		@NotNull
		private Integer idfArtist;
		@NotNull
		private MultipartFile profileImage;
	}
	
}
