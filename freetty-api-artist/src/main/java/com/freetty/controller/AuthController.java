package com.freetty.controller;

import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.mobile.device.Device;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.freetty.common.ImageUtil;
import com.freetty.dto.AuthDto;
import com.freetty.dto.JsonDto;
import com.freetty.service.AuthServiceImpl;

@RestController
@RequestMapping("/auth")
public class AuthController extends BaseController {

	@Autowired
	AuthServiceImpl authService;
	
	@Autowired
	ImageUtil imageUtil;
	
	@Value("${cloud.aws.s3.bucket}")
	private String bucketName;
	

	@RequestMapping(value = "/signin", method = RequestMethod.POST)
	public ResponseEntity<?> signin(@RequestBody AuthDto authDto, HttpServletResponse res, Device device) {
		JsonDto<AuthDto> jDto = authService.signin(authDto, res, device);
		logger.info(device.getDevicePlatform().name());
		return new ResponseEntity<>(jDto, HttpStatus.OK);
	}

	@RequestMapping(value = "/signup", method = RequestMethod.POST)
	public ResponseEntity<?> signup(@RequestBody AuthDto authDto) {
		JsonDto<AuthDto> jDto = authService.signup(authDto);
		return new ResponseEntity<>(jDto, HttpStatus.OK);
	}

	@RequestMapping(value = "/validateEmail", method = RequestMethod.POST)
	public ResponseEntity<?> validateEmail(@RequestBody AuthDto.ValidateEmail authDtoValidateEmail) {
		JsonDto<AuthDto.ValidateEmail> jDto = authService.validateEmail(authDtoValidateEmail);
		return new ResponseEntity<>(jDto, HttpStatus.OK);
	}

	@Secured("IS_AUTHENTICATED_FULLY")
	@RequestMapping(value = "/test", method = RequestMethod.GET)
	public ResponseEntity<?> authTest() {
		return new ResponseEntity<>("Success", HttpStatus.OK);
	}

	@RequestMapping(value = "/uploadTest", method = RequestMethod.POST)
	public List<String> uploadTest(MultipartFile[] images) {
		return imageUtil.uploadImage(bucketName, "profile", "1", images);
	}

	
}
