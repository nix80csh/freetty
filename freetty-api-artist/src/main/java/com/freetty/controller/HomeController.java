package com.freetty.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.freetty.entity.Customer;
import com.freetty.repository.CustomerRepo;
 
@RestController
@RequestMapping("/home")
public class HomeController extends BaseController {	
	@Autowired CustomerRepo customerRepo;
	
	@RequestMapping("/")
	public Customer home(){
		Customer customer = customerRepo.findByEmail("chosh@freetty.com");
		return customer;
	}
	
	@RequestMapping("/in")
	public String permitTestIn(){		
		return "8080";
	}
	
	@RequestMapping("/out")
	public String permitTestOut(){		
		return "접근허용페이지";
	}
	
			
}
