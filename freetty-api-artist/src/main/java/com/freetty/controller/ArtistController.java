package com.freetty.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.freetty.dto.ArtistDto;
import com.freetty.dto.ArtistScheduleDto;
import com.freetty.dto.ArtistShopDto;
import com.freetty.dto.JsonDto;
import com.freetty.service.ArtistServiceImpl;

@RestController
@RequestMapping("/artist")
public class ArtistController extends BaseController{

	
	@Autowired ArtistServiceImpl artistService; 
	
	@RequestMapping(value="/registProfile", method=RequestMethod.POST)
	public ResponseEntity<?> registProfile(@RequestBody ArtistDto artistDto) {
		JsonDto<ArtistDto> jDto = artistService.registProfile(artistDto);	
		return new ResponseEntity<>(jDto, HttpStatus.OK);
	}
	
	@RequestMapping(value="/registProfileImage", method=RequestMethod.POST)
	public ResponseEntity<?> registProfileImage(@Valid ArtistDto.saveProfileImage profileImageDto) {
		JsonDto<ArtistDto> jDto = artistService.registProfileImage(profileImageDto);	
		return new ResponseEntity<>(jDto, HttpStatus.OK);
	}
	
	
	@RequestMapping(value="/registShop", method=RequestMethod.POST)
	public ResponseEntity<?> registShop(@RequestBody ArtistShopDto artistShopDto) {
		JsonDto<ArtistShopDto> jDto = artistService.registShop(artistShopDto);	
		return new ResponseEntity<>(jDto, HttpStatus.OK);
	}
	
	@RequestMapping(value="/registSchedule", method=RequestMethod.POST)
	public ResponseEntity<?> registSchedule(@RequestBody ArtistScheduleDto artistScheduleDto) {
		JsonDto<ArtistScheduleDto> jDto = artistService.registSchedule(artistScheduleDto);	
		return new ResponseEntity<>(jDto, HttpStatus.OK);
	}
	
	
}
