package com.freetty.service;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mobile.device.Device;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.freetty.common.EmailValidUtil;
import com.freetty.common.TokenUtil;
import com.freetty.dto.AuthDto;
import com.freetty.dto.JsonDto;
import com.freetty.entity.Artist;
import com.freetty.repository.ArtistRepo;

@Service
@Transactional
public class AuthServiceImpl {

	@Autowired
	AuthenticationManager authenticationManager;
	@Autowired
	TokenUtil tokenUtil;

	@Autowired
	ArtistRepo artistRepo;

	@Autowired
	EmailValidUtil emailValidUtil;
	
	@Value("${freetty.token.header}")
	private String tokenHeader;

	private final Logger logger = Logger.getLogger(this.getClass());

	public JsonDto<AuthDto> signin(AuthDto authDto, HttpServletResponse res, Device device) {
		JsonDto<AuthDto> jDto = new JsonDto<AuthDto>();

		try {
			// 인증
			UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken(authDto.getEmail(),
					authDto.getPassword());
			Authentication authentication = authenticationManager.authenticate(token);
			SecurityContextHolder.getContext().setAuthentication(authentication);

			// 토큰생성
			Artist artist = artistRepo.findByEmail(authDto.getEmail());
			String xAuthToken = tokenUtil.createToken(artist.getEmail(), artist.getPassword());

			// 쿠키생성
			Cookie cookie = new Cookie(this.tokenHeader, xAuthToken);
			res.addCookie(cookie);

			authDto.setIdfArtist(artist.getIdfArtist());
			authDto.setPassword(null);

			jDto.setResultCode("S");
			jDto.setDataObject(authDto);

		} catch (BadCredentialsException e) {
			jDto.setResultCode("F");
			jDto.setResultMessage("Invalid Email or Password");
		}
		return jDto;
	}

	public JsonDto<AuthDto> signup(AuthDto authDto) {
		JsonDto<AuthDto> jDto = new JsonDto<AuthDto>();

		if (artistRepo.findByEmail(authDto.getEmail()) == null) {
			logger.info("null");
			Artist artist = new Artist();
			artist.setEmail(authDto.getEmail());
			artist.setPassword(authDto.getPassword());
			artistRepo.save(artist);
			jDto.setResultCode("S");

		} else {
			jDto.setResultCode("F");
			jDto.setResultMessage("exist artist");
			logger.info("exist");

		}

		return jDto;
	}

	public JsonDto<AuthDto.ValidateEmail> validateEmail(AuthDto.ValidateEmail authDtoValidateEmail) {
		JsonDto<AuthDto.ValidateEmail> jDto = new JsonDto<AuthDto.ValidateEmail>();
		jDto.setResultCode(emailValidUtil.checkValid(authDtoValidateEmail.getEmail()));
		return jDto;
	}

}
