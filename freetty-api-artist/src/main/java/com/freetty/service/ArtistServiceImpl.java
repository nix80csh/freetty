package com.freetty.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.freetty.common.BeanUtil;
import com.freetty.common.ImageUtil;
import com.freetty.dto.ArtistDto;
import com.freetty.dto.ArtistScheduleDto;
import com.freetty.dto.ArtistShopDto;
import com.freetty.dto.JsonDto;
import com.freetty.entity.Artist;
import com.freetty.entity.ArtistSchedule;
import com.freetty.entity.ArtistShop;
import com.freetty.repository.ArtistRepo;
import com.freetty.repository.ArtistScheduleRepo;
import com.freetty.repository.ArtistShopRepo;

@Service
@Transactional
public class ArtistServiceImpl {

	@Autowired
	ArtistRepo artistRepo;
	@Autowired
	ArtistShopRepo artistShopRepo;
	@Autowired
	ArtistScheduleRepo artistScheduleRepo;
	@Autowired
	ImageUtil imageUtil;
	@Value("${cloud.aws.s3.bucket}")
	private String bucketName;

	public JsonDto<ArtistDto> registProfile(ArtistDto artistDto) {
		JsonDto<ArtistDto> jDto = new JsonDto<ArtistDto>();

		if (artistRepo.exists(artistDto.getIdfArtist())) {
			Artist artist = artistRepo.findOne(artistDto.getIdfArtist());
			BeanUtil.copyProperties(artistDto, artist);
			artistRepo.save(artist);
			BeanUtil.copyProperties(artist, artistDto);
			System.out.println(artistDto.getGender());
			// Transactional Test
			// if (artistDto.getGender().equals("F")) {
			// throw new RuntimeException("roll back");
			// }

			jDto.setResultCode("S");
			jDto.setDataObject(artistDto);
		} else {
			jDto.setResultCode("F");
			jDto.setResultMessage("does not exist");
		}
		return jDto;
	}

	public JsonDto<ArtistShopDto> registShop(ArtistShopDto artistShopDto) {

		JsonDto<ArtistShopDto> jDto = new JsonDto<ArtistShopDto>();
		ArtistShop artistShop = new ArtistShop();
		BeanUtil.copyProperties(artistShopDto, artistShop);
		artistShopRepo.save(artistShop);
		BeanUtil.copyProperties(artistShop, artistShopDto);
		jDto.setResultCode("S");
		jDto.setDataObject(artistShopDto);

		return jDto;
	}

	public JsonDto<ArtistScheduleDto> registSchedule(ArtistScheduleDto artistScheduleDto) {

		JsonDto<ArtistScheduleDto> jDto = new JsonDto<ArtistScheduleDto>();
		ArtistSchedule artistSchedule = new ArtistSchedule();
		BeanUtil.copyProperties(artistScheduleDto, artistSchedule);
		artistScheduleRepo.save(artistSchedule);
		BeanUtil.copyProperties(artistSchedule, artistScheduleDto);
		jDto.setResultCode("S");
		jDto.setDataObject(artistScheduleDto);

		return jDto;
	}

	public JsonDto<ArtistDto> registProfileImage(ArtistDto.saveProfileImage profileImageDto) {
		JsonDto<ArtistDto> jDto = new JsonDto<ArtistDto>();

		if (artistRepo.findOne(profileImageDto.getIdfArtist()) == null) {
			jDto.setResultCode("F");
			jDto.setResultMessage("does not exist artist");
		} else {

			String fileName = profileImageDto.getProfileImage().getOriginalFilename();
			int index = fileName.lastIndexOf(".");
			String extension = fileName.substring(index + 1);
			if (extension.equals("jpg")) {
				String idfArtist = profileImageDto.getIdfArtist().toString();
				imageUtil.uploadImage(bucketName, "profile", idfArtist, profileImageDto.getProfileImage());
				jDto.setResultCode("S");
			} else {
				jDto.setResultCode("F");
				jDto.setResultMessage("does not support extension");
			}
		}
		return jDto;
	}

}
